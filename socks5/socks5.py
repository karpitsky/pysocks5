import struct
import urlparse
import sys
from commons import *

__all__ = [
    "SOCKS5",
    "set_default_socks_proxy",
    "set_default_rdns",
    "set_default_debug",
    "set_default_auth",
]


methods_client_supported = [
    METHOD_NO_AUTHENTICATION_REQUIRED,
    METHOD_USERNAME_PASSWORD,
]

cmds_client_supported = [
    CMD_CONNECT,
]


_origin_socket = socket.socket
_default_proxy = None
_default_rdns = True
_default_debug = 0
_default_username = None
_default_password = None

def set_default_socks_proxy(proxies):
    global _default_proxy
    _default_proxy = proxies

def set_default_rdns(rdns = True):
    global _default_rdns
    _default_rdns = rdns

def set_default_debug(enable):
    global _default_debug
    _default_debug = enable

def set_default_auth(username, password):
    global _default_username
    global _default_password
    _default_username = username
    _default_password = password


def parse_host_port_from_uri(uri):
    parses = urlparse.urlparse(uri)
    splits = parses.netloc.split(":")

    if len(splits) == 2:
        host, port = splits[0], int(splits[1])
    else:
        host = splits[0]

        if parses.scheme == "http":
            port = 80
        elif parses.schema == "https":
            port = 443
        else:
            raise TypeError("parse port from URI failed")

    return host, port

def validate_field_rep(field_rep):
    is_valid = lambda field_rep : (field_rep in replies) or\
                                  (REP_UNASSIGNED_B <= field_rep <= REP_UNASSIGNED_E)

    if not is_valid(field_rep):
        buf = ", ".join(hex(i) for i in replies.keys())
        msg = "expected one of {%s}, got %d" % (buf, field_rep)
        raise NotConfirmsToProtocol(message = msg)


class SOCKS5(socket.socket):
    def __init__(self, family = socket.AF_INET, type = socket.SOCK_STREAM,
                 proto = 0, _sock = None):
        _origin_socket.__init__(self, family = family, type = type,
            proto = proto, _sock = _sock)

        if _default_proxy:
            splits = _default_proxy.split(":")

            if len(splits) == 2:
                self._proxy_host, self._proxy_port = splits[0], int(splits[1])
            else:
                self._proxy_host, self._proxy_port = splits[0], default_port
        else:
            self._proxy_host = None
            self._proxy_port = None

        self._rdns = _default_rdns
        self._debug = _default_debug

        self._username = _default_username
        self._password = _default_password

    def set_auth(self, username, password):
        self._username = username
        self._password = password

    def set_socks_proxy(self, proxy, rdns = True):
        self._proxy = proxy
        self._rdns = rdns

    def _log_client_info(self):
        if self._debug:
            remote_host, remote_port = self._proxy_host, self._proxy_port
            msg = "[%s] connect to server %s:%d" % (get_ts(), remote_host, remote_port)
            sys.stdout.write(msg + "\n")

    def connect(self, address):
        host, port = address[0], address[1]

        if self._proxy_host:
            _origin_socket.connect(self, (self._proxy_host, self._proxy_port))

            self._log_client_info()

            self._method_negotiate(host, port)
        else:
            _origin_socket.connect(self, (host, port))
            
    def _auth_negotiate(self):
        VER = 0x01
        data = chr(VER) + chr(len(self._username)) + self._username +\
               chr(len(self._password)) + self._password
        self.sendall(data)

        data = self.recv(2)
        field_ver, field_status = ord(data[0]), ord(data[1])

        validate_field_ver(field_ver)
        if field_status == AUTH_SUCCESS:

            if self._debug:
                msg = "    authentication successfully"
                sys.stdout.write(msg + "\n")

        elif field_status == AUTH_FAILURE:
            raise AuthenticationFailure

    def _method_negotiate(self, host, port):
        field_nmethods = len(methods_client_supported)
        field_methods = "".join([chr(i) for i in methods_client_supported])
        data = chr(SOCKS_VERSION_5) + chr(field_nmethods) + field_methods
        self.sendall(data)

        data = self.recv(2)
        field_ver, field_method = ord(data[0]), ord(data[1])
        field_methods = chr(field_method)

        validate_field_ver(field_ver)
        validate_field_methods(field_methods)

        if field_method not in methods_client_supported:
            buf = ", ".join([hex(i) for i in methods_client_supported])
            msg = "expected one of {%s}, got %d" % (buf, field_method)
            msg += "client doesn't supports method %d" % (field_method)
            raise NotImplemented(message = msg)

        if field_method == METHOD_USERNAME_PASSWORD:
            self._auth_negotiate()

        self._cmd_negotiate(host, port)

    def _cmd_negotiate(self, host, port):
        if validate_ip_address(host):
            dst_addr, dst_port = socket.inet_aton(host), struct.pack("!H", port)
            req = chr(SOCKS_VERSION_5) + chr(CMD_CONNECT) + chr(FIELD_RSV) +\
                  chr(ATYP_IPV4) + dst_addr + dst_port

        elif validate_hostname(host):
            if self._rdns:
                dst_addr, dst_port = host, struct.pack("!H", port)

                req = chr(SOCKS_VERSION_5) + chr(CMD_CONNECT) + chr(FIELD_RSV) +\
                      chr(ATYP_DOMAIN_NAME) + chr(len(dst_addr)) + dst_addr + dst_port
            else:
                host = socket.gethostbyname(host)
                dst_addr, dst_port = socket.inet_aton(host), struct.pack("!H", port)

                req = chr(SOCKS_VERSION_5) + chr(CMD_CONNECT) + chr(FIELD_RSV) +\
                      chr(ATYP_IPV4) + dst_addr + dst_port
        else:
            raise NotImplemented

        self.sendall(req)


        data = self.recv(4)
        if not data:
            msg = "expected 4 bytes length of response, got %d" % len(data)
            raise SOCKS5Exception(msg)

        field_ver, field_rep, field_rsv, field_atyp = ord(data[0]), ord(data[1]), \
                                                      ord(data[2]), ord(data[3])

        validate_field_ver(field_ver)
        validate_field_rep(field_rep)
        validate_field_rsv(field_rsv)
        validate_field_atyp(field_atyp)

        if field_rep != REP_SUCCEEDED:
            msg = replies[REP_UNASSIGNED_B]
            raise SOCKS5Exception(msg)

        if self._debug:
            msg = "    negotiation successfully"
            sys.stdout.write(msg + "\n")


        if field_atyp == ATYP_IPV4:
            bnd_addr = self.recv(4)
            bnd_addr = socket.inet_ntoa(bnd_addr)
            bnd_port = self.recv(2)

        elif field_atyp == ATYP_DOMAIN_NAME:
            domain_name_len = self.recv(1)
            bnd_addr = self.recv(domain_name_len)
            bnd_addr = socket.gethostbyname(bnd_addr)

            bnd_port = self.recv(2)

        elif atyp == ATYP_IPV6:
            raise NotImplemented

        bnd_port = struct.unpack("!H", bnd_port)
        bnd_port = bnd_port[0]

        if bnd_addr == INADDR_ANY and bnd_port == 0:
            pass


if __name__ == "__main__":
    set_default_debug(1)

    proxies = "127.0.0.1:7070"
    set_default_socks_proxy(proxies = proxies)

    fields = [
        "GET / HTTP/1.1",
        "User-Agent: curl/7.19.7 (universal-apple-darwin10.0) libcurl/7.19.7 OpenSSL/0.9.8r zlib/1.2.3",
        "Host: en.wikipedia.org",
        "Accept: */*"
        ]
    req = CRLF.join(fields) + CRLF * 2

    sock = SOCKS5()
#    host, port = "en.wikipedia.org", 80
    host, port = "localhost", 80
    sock.connect((host, port))

    sock.sendall(req)
    resp = sock.recv(1024)

    print resp