import re

from commons import CRLF

__all__ = [
    "Headers",
    "get_http_message",
]


class Headers(object):
    pattern = "(?P<key>[a-z\-].+?):\s*(?P<value>[a-z0-9 -_\/.]+?)" + CRLF
    def __init__(self, fromstring):

        self._headers = {}

        # confirms to RFC 2616 Hypertext Transfer Protocol -- HTTP/1.1,
        # section '4.2 Message Headers', 'Field names are case-insensitive.'.
        self._headers_lower = {}

        self._parse_from_string(fromstring)

    def _parse_from_string(self, string):
        headers = re.findall(pattern = self.pattern, string = string, flags = re.IGNORECASE | re.MULTILINE)
        self._headers = dict(headers)

        for key in self._headers:
            self._headers_lower[key.lower()] = self._headers[key]

    @property
    def headers(self):
        return self._headers

    @property
    def headers_lower(self):
        return self._headers_lower

    def get_value_by_key(self, name):
        name = name.lower()

        if name in self._headers_lower:
            return self._headers_lower[name]



def get_http_message(sock):
    shortest_req = "GET / HTTP/1.0" + CRLF * 2
    shortest_req_len = len(shortest_req)

    buf = sock.recv(shortest_req_len)
    while buf.find(CRLF * 2) == -1:
        buf += sock.recv(1)

    headers_obj = Headers(buf)
    msg_body_len = headers_obj.get_value_by_key("Content-Length")

    if msg_body_len is not None:
        msg_body_len = int(msg_body_len)
        msg_body = sock.recv(msg_body_len)

        buf += msg_body

    return buf


if __name__ == "__main__":
    fields = [
        "GET / HTTP/1.1",
        "User-Agent: curl/7.19.7 (universal-apple-darwin10.0) libcurl/7.19.7 OpenSSL/0.9.8r zlib/1.2.3",
        "Host: localhost",
        "Accept: */*",
        ]
    buf = CRLF.join(fields) + CRLF * 2

    headers_obj = Headers(fromstring = buf)


    expected_headers = {
        "User-Agent" : "curl/7.19.7 (universal-apple-darwin10.0) libcurl/7.19.7 OpenSSL/0.9.8r zlib/1.2.3",
        "Host" : "localhost",
        "Accept" : "*/*",
    }

    got = headers_obj.headers

    assert got == expected_headers
    assert "user-agent" in headers_obj.headers_lower
    assert "host" in headers_obj.headers_lower
    assert "accept" in headers_obj.headers_lower
