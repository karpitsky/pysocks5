import re
import socket
import time

CRLF = "\r\n"

INADDR_ANY = "0.0.0.0"

SOCKS_VERSION_5 = 0x05

METHOD_NO_AUTHENTICATION_REQUIRED = 0x00
METHOD_GSSAPI = 0x01
METHOD_USERNAME_PASSWORD = 0x02
METHOD_IANA_ASSIGNED_B = 0x03
METHOD_IANA_ASSIGNED_E = 0x7F
METHOD_RESERVED_FOR_PRIVATE_METHODS_B = 0x80
METHOD_RESERVED_FOR_PRIVATE_METHODS_E = 0xFE
METHOD_NO_ACCEPTABLE_METHODS = 0xFF

methods = {
    METHOD_NO_AUTHENTICATION_REQUIRED : "NO AUTHENTICATION REQUIRED",
    METHOD_GSSAPI : "GSSAPI",
    METHOD_USERNAME_PASSWORD : "USERNAME/PASSWORD",
#    METHOD_IANA_ASSIGNED_B : "IANA ASSIGNED",
#    METHOD_IANA_ASSIGNED_E : "IANA ASSIGNED",
#    METHOD_RESERVED_FOR_PRIVATE_METHODS_B : "RESERVED FOR PRIVATE METHODS",
#    METHOD_RESERVED_FOR_PRIVATE_METHODS_E : "RESERVED FOR PRIVATE METHODS",
    METHOD_NO_ACCEPTABLE_METHODS : "NO ACCEPTABLE METHODS",
}

methods_supported = {
    METHOD_NO_AUTHENTICATION_REQUIRED : "NO AUTHENTICATION REQUIRED",
    METHOD_USERNAME_PASSWORD : "USERNAME/PASSWORD"
}

CMD_CONNECT = 0x01
CMD_BIND = 0x02
CMD_UDP = 0x03
cmds = {
    CMD_CONNECT : "CONNECT",
    CMD_BIND : "BIND",
    CMD_UDP : "UDP",
}

ATYP_IPV4 = 0x01
ATYP_DOMAIN_NAME = 0x03
ATYP_IPV6 = 0x04

atyps = {
    ATYP_IPV4 : "IPv4",
    ATYP_DOMAIN_NAME : "DomainName",
    ATYP_IPV6 : "IPv6",
}

REP_SUCCEEDED = 0x0
REP_GENERAL_SOCKS_SERVER_FAILURE = 0x01
REP_CONNECTION_NOT_ALLOWED_BY_RULE_SET = 0x02
REP_NETWORK_UNREACHABLE = 0x03
REP_HOST_UNREACHABLE = 0x04
REP_CONNECTION_REFUSED = 0x05
REP_TTL_EXPIRED = 0x06
REP_COMMAND_NOT_SUPPORTED = 0x07
REP_ADDRESS_TYPE_NOT_SUPPORTED = 0x08
REP_UNASSIGNED_B = 0x09
REP_UNASSIGNED_E = 0xFF

replies = {
    REP_SUCCEEDED : "succeeded",
    REP_GENERAL_SOCKS_SERVER_FAILURE : "general SOCKS server failure",
    REP_CONNECTION_NOT_ALLOWED_BY_RULE_SET : "connection not allowed by rule set",
    REP_NETWORK_UNREACHABLE : "Network unreachable",
    REP_HOST_UNREACHABLE : "Host unreachable",
    REP_CONNECTION_REFUSED : "Connection refused",
    REP_TTL_EXPIRED : "TTL expired",
    REP_COMMAND_NOT_SUPPORTED : "Command not supported",
    REP_ADDRESS_TYPE_NOT_SUPPORTED : "Address type not supported",
#    REP_UNASSIGNED_B : "unassigned",
#    REP_UNASSIGNED_E : "unassigned",
}

class SOCKS5Exception(Exception):
    def __init__(self, message = ""):
        self.args = (message, )
        self.message = message

class UnexpectedProtocolVersion(SOCKS5Exception):
    def __init__(self, message):
        self.args = (message, )
        self.message = message

class NotImplemented(SOCKS5Exception):
    def __init__(self, message = ""):
        self.args = (message, )
        self.message = message

class NotConfirmsToProtocol(SOCKS5Exception):
    def __init__(self, message = ""):
        self.args = (message, )
        self.message = message

class AuthenticationFailure(SOCKS5Exception):
    def __init__(self, message = ""):
        self.args = (message, )
        self.message = message

# for backwards compatibility
error = SOCKS5Exception


default_port = 1080

AUTH_SUCCESS = 0x0
AUTH_FAILURE = 0x1

FIELD_RSV = 0x0

FIELD_NMETHODS_MIN = 0x01
FIELD_NMETHODS_MAX = 0xFF

FIELD_ULEN_MIN = 0x01
FIELD_ULEN_MAX = 0xFF

FIELD_PLEN_MIN = 0x01
FIELD_PLEN_MAX = 0xFF


def get_ts(in_locale = True):
    if in_locale:
        return time.strftime("%Y-%m-%d %H:%M", time.localtime(time.time()))
    else:
        return time.strftime("%Y-%m-%d %H:%M", time.gmtime(time.time()))

def validate_field_atyp(field_atyp):
    if field_atyp not in atyps.keys():
        buf = ", ".join(hex(i) for i in atyps.keys())
        msg = "expected one of {%s}, got %s" % (buf, hex(field_atyp))
        raise NotConfirmsToProtocol(message = msg)

def validate_field_methods(field_methods, methods_supported = None):
    is_inna_assigned = lambda field : (METHOD_IANA_ASSIGNED_B <= method and
                                       method <= METHOD_IANA_ASSIGNED_E)
    is_reserved_for_private = lambda field : (METHOD_RESERVED_FOR_PRIVATE_METHODS_B <= method and
                                              method <= METHOD_IANA_ASSIGNED_E)

    for i in field_methods:
        method = ord(i)
        if (method not in methods.keys()) and\
           (not is_inna_assigned(method)) and\
           (not is_reserved_for_private(method)):

            if methods_supported:
                buf = ", ".join(hex(i) for i in methods_supported)
            else:
                buf = ", ".join(hex(i) for i in (methods.keys()))
                buf = "%s, %s~%s, %s~%s" % (buf,
                                            hex(METHOD_IANA_ASSIGNED_B),
                                            hex(METHOD_IANA_ASSIGNED_E),
                                            hex(METHOD_RESERVED_FOR_PRIVATE_METHODS_B),
                                            hex(METHOD_RESERVED_FOR_PRIVATE_METHODS_E))
            msg = "    expected one of {%s}, got %s" % (buf, hex(method))
            raise NotConfirmsToProtocol(message = msg)

def validate_field_rsv(field_rsv):
    if field_rsv != FIELD_RSV:
        msg = "expected field RSV is equal to %d, got %d" % (FIELD_RSV, field_rsv)
        raise NotConfirmsToProtocol(message = msg)

def validate_field_ver(field_ver):
    if field_ver != SOCKS_VERSION_5:
        msg = "expected version %d, got %d" % (SOCKS_VERSION_5, field_ver)
        raise UnexpectedProtocolVersion(message = msg)


def validate_ip_address(string):
    """
    >>> validate_ip_address('127.0.0.1')
    True
    >>> validate_ip_address('208.80.152.201')
    True
    >>> validate_ip_address('google.com')
    False
    >>> validate_ip_address('localhost')
    False
    >>> validate_ip_address('::1')
    False
    """
    try:
        socket.inet_aton(string)
    except socket.error:
        return False

    return True

def validate_hostname(string):
    """ Copy from
    http://stackoverflow.com/questions/2532053/validate-hostname-string-in-python

    >>> validate_hostname('127.0.0.1')
    True
    >>> validate_hostname('208.80.152.201')
    True
    >>> validate_hostname('google.com')
    True
    >>> validate_hostname('localhost')
    True
    >>> validate_hostname('::1')
    False
    """
    if len(string) > 255:
        return False

    if string[-1:] == ".":
        string = string[:-1] # strip exactly one dot from the right, if present

    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)

    return all(allowed.match(x) for x in string.split("."))



if __name__ == "__main__":
    import doctest
    doctest.testmod()