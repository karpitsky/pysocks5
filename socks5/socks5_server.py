#!/usr/bin/env python
"""
SOCKS Protocol Version 5 Python Implementation

References

 - http://tools.ietf.org/rfc/rfc1928.txt
 - http://tools.ietf.org/rfc/rfc1929.txt
 - http://tools.ietf.org/rfc/rfc2616.txt
"""
import SocketServer
import os
import struct
import sys
import errno

from commons import *
from htmlutil import get_http_message

__all__ = [
    "SOCKS5Server",
]


TERMINATE_CONN = None

methods_server_supported = [
#    METHOD_NO_AUTHENTICATION_REQUIRED,
    METHOD_USERNAME_PASSWORD,
]

cmds_server_supported = [
    CMD_CONNECT,
]


def validate_field_nmethods(field_nmethods):
    if not ((FIELD_NMETHODS_MIN <= field_nmethods) and
            (field_nmethods <= FIELD_NMETHODS_MAX)):
        msg = "expected %d <= field NMETHODS <= %d, got %d" %\
              (FIELD_NMETHODS_MIN, FIELD_NMETHODS_MAX, field_nmethods)
        raise NotConfirmsToProtocol(message = msg)

def validate_field_cmd(field_cmd):
    if field_cmd not in cmds.keys():
        buf = ", ".join(hex(i) for i in cmds.keys())
        msg = "expected one of {%s}, got %s" % (buf, hex(field_cmd))
        raise NotConfirmsToProtocol(message = msg)

def _load_username_password():
    auth_path = os.path.join(os.getenv("HOME"), ".config", "pysocks5", "auth")
    parent = os.path.dirname(os.path.realpath(auth_path))

    if not os.path.exists(parent):
        os.makedirs(parent)

    mapping = {}
    if os.path.exists(auth_path):
        msg = "    load %s" % auth_path
        sys.stdout.write(msg + "\n")

        lines = file(auth_path).read().split()
        for line in lines:
            if line:
                if line.find(":") == -1:
                    msg = "expected 'username:password' per line, got '%s'" % line
                    raise Exception(msg)

                splits = line.split(":")
                mapping[splits[0]] = splits[1]

    return mapping


class SOCKS5Server(SocketServer.ForkingMixIn, SocketServer.TCPServer):
    allow_reuse_address = True

    
class MyTCPHandler(SocketServer.BaseRequestHandler):
    def setup(self):
        self._dst_sock = None
        self._log_client_info()

    def _log_client_info(self):
        remote_host, remote_port = self.client_address[0], self.client_address[1]
        msg = "[%s] %s:%d connected" % (get_ts(), remote_host, remote_port)
        sys.stdout.write(msg + "\n")

    def _username_passwd_auth(self):
        data = self.request.recv(2)
        field_ver, field_ulen = ord(data[0]), ord(data[1])

        assert field_ver == 0x01


        if not ((FIELD_ULEN_MIN <= field_ulen) and (field_ulen <= FIELD_ULEN_MAX)):
            msg = "expected %d < field ULEN < %d, got %d" % \
                  (FIELD_ULEN_MIN, FIELD_ULEN_MAX, field_ulen)
            raise NotConfirmsToProtocol(message = msg)

        got_username = self.request.recv(field_ulen)

        if got_username in _auth_mapping:
            expected_password = _auth_mapping[got_username]
        else:
            resp = chr(SOCKS_VERSION_5) + chr(AUTH_FAILURE)
            self.request.sendall(resp)
            return AUTH_FAILURE

        data = self.request.recv(1)
        field_plen = ord(data)

        if not ((FIELD_PLEN_MIN <= field_plen) and (field_plen <= FIELD_PLEN_MAX)):
            msg = "expected %d < field PLEN < %d, got %d" % \
                  (FIELD_PLEN_MIN, FIELD_PLEN_MAX, field_plen)
            raise NotConfirmsToProtocol(message = msg)

        got_passwd = self.request.recv(field_plen)

        if expected_password == got_passwd:
            rep = AUTH_SUCCESS
        else:
            rep = AUTH_FAILURE

            msg = "    user '%s' authentication failed" % got_username
            sys.stdout.write(msg + "\n")

        resp = chr(SOCKS_VERSION_5) + chr(rep)
        self.request.sendall(resp)

        return rep

    def _consume_http_request(self):
        buf_req = get_http_message(self.request)
        msg = "    recv HTTP request from client, length: %d" % len(buf_req)
        sys.stdout.write(msg + "\n")
        self._dst_sock.sendall(buf_req)

        buf_resp = get_http_message(self._dst_sock)
        msg = "    recv HTTP response from target host, length: %d" % len(buf_resp)
        sys.stdout.write(msg + "\n")
        self.request.sendall(buf_resp)

    def _auth(self, field_methods):
        methods_client_supported = []
        for i in field_methods:
            method = ord(i)
            methods_client_supported.append(method)

        if (METHOD_NO_AUTHENTICATION_REQUIRED in methods_client_supported) and \
           (METHOD_NO_AUTHENTICATION_REQUIRED in methods_server_supported):
            field_rep = METHOD_NO_AUTHENTICATION_REQUIRED

        elif (METHOD_USERNAME_PASSWORD in methods_client_supported) and \
             (METHOD_USERNAME_PASSWORD in methods_server_supported):
            field_rep = METHOD_USERNAME_PASSWORD
            
        else:
            field_rep = METHOD_NO_ACCEPTABLE_METHODS

        resp = chr(SOCKS_VERSION_5) + chr(field_rep)
        self.request.sendall(resp)

        if field_rep == METHOD_NO_AUTHENTICATION_REQUIRED:
            return True
        if field_rep == METHOD_USERNAME_PASSWORD:
            result = self._username_passwd_auth()
            if result == AUTH_SUCCESS:

                msg = "    username/password authentication successfully"
                sys.stdout.write(msg + "\n")

                return True
            else:
                msg = "    username/password authentication failed"
                sys.stdout.write(msg + "\n")

        return False

    def handle(self):
        data = self.request.recv(2)
        field_ver, field_nmethods = ord(data[0]), ord(data[1])

        validate_field_ver(field_ver)
        validate_field_nmethods(field_nmethods)


        data = self.request.recv(field_nmethods)
        field_methods = data

        validate_field_methods(field_methods)

        if not self._auth(field_methods):
            return TERMINATE_CONN


        data = self.request.recv(4)
        field_ver, field_cmd, field_rsv, field_atyp = ord(data[0]), ord(data[1]), \
                                                      ord(data[2]), ord(data[3])
        
        validate_field_ver(field_ver)
        validate_field_cmd(field_cmd)
        validate_field_rsv(field_rsv)
        validate_field_atyp(field_atyp)

        if field_cmd in cmds_server_supported:
            field_rep = REP_SUCCEEDED

            msg = "    CMD: %s" % cmds[field_cmd]
            sys.stdout.write(msg + "\n")
        else:
            field_rep = REP_COMMAND_NOT_SUPPORTED


        dst_addr = INADDR_ANY

        if field_atyp == ATYP_IPV4:
            field_dst_addr = self.request.recv(4)
            dst_addr = socket.inet_ntoa(field_dst_addr)

        elif field_atyp == ATYP_DOMAIN_NAME:
            data = self.request.recv(1)
            domain_name_len = ord(data)

            field_dst_addr = self.request.recv(domain_name_len)

            try:
                dst_addr = socket.gethostbyname(field_dst_addr)
            except socket.gaierror:
                field_rep = REP_NETWORK_UNREACHABLE

        elif field_atyp == ATYP_IPV6:
            field_dst_addr = self.request.recv(16)
            # TBD...
            raise NotImplemented

        field_dst_port = self.request.recv(2)
        dst_port = struct.unpack("!H", field_dst_port)[0]

        msg = "    target host: %s:%d" % (dst_addr, dst_port)
        sys.stdout.write(msg + "\n")


        if field_rep == REP_SUCCEEDED:
            self._dst_sock = socket.socket()

            try:
                self._dst_sock.connect((dst_addr, dst_port))
            except socket.error, ex:
                if ex[0] == errno.ECONNREFUSED:
                    field_rep = REP_CONNECTION_REFUSED
                elif ex[0] == errno.ETIMEDOUT:
                    field_rep = REP_HOST_UNREACHABLE
                else:
                    print "ex:", ex
                    field_rep = REP_GENERAL_SOCKS_SERVER_FAILURE

        msg = "    reply: %s" % replies[field_rep]
        sys.stdout.write(msg + "\n")

        bnd_addr, bnd_port = INADDR_ANY, 0
        resp = chr(SOCKS_VERSION_5) + chr(field_rep) + chr(FIELD_RSV) + \
               chr(field_atyp) + \
               socket.inet_aton(bnd_addr) + struct.pack("!H", bnd_port)

        self.request.send(resp)
        
        if field_rep != REP_SUCCEEDED:
            return TERMINATE_CONN


        self._consume_http_request()

if __name__ == "__main__":
    global _auth_mapping
    _auth_mapping = _load_username_password()

    host, port = "0.0.0.0", default_port

    msg = "%s:%d" % (host, port)
    print msg

    srv = SOCKS5Server((host, port), MyTCPHandler)
    srv.serve_forever()
