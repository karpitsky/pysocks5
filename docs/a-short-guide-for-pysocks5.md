# PySOCKS5 USAGE

## Simple SOCKS5 Proxy Server

play it in a virtual environment

    virtualenv /tmp/sandbox
    source /tmp/sandbox/bin/activate

    git clone https://bitbucket.org/shugelee/pysocks5.git
    cd pysocks5
    /tmp/sandbox/bin/python setup.py develop

    cd /tmp/sandbox

    python -msocks5.socks5_server


test no authentication required

    curl --socks5-hostname 127.0.0.1:1080 http://en.wikipedia.org/

## SOCKS5 Proxy Server with username/password authentication(RFC 1929)

create

    $HOME/.config/pysocks5/auth ,

append

    username:password

per-line into it,


re-start the module

    python -msocks5.socks5_server


test it (assume you has appended 'girl:xx00' into $HOME/.config/pysocks5/auth )

    curl --socks5-hostname 127.0.0.1:1080 --proxy-user girl:xx00  http://en.wikipedia.org/


## Using it in Client

PySOCKS5 could breaks the G#reat F#ireW#all, if you use it without httplib/urllib/urllib2.


setup SOCKS5 Proxy server on local:

    ssh -f -C -q -N -D 7070 $user@$proxy_server


code snippet:

    import socks5
    socks5.set_default_socks_proxy("127.0.0.1:7070")
    socket.socket = socks5.SOCKS5

    sock = socket.socket()
    req = "GET / HTTP/1.0\r\nHost: t#witter#.com\r\n...."
    sock.send(req)
    # ...


## Using it in Client with username/password authentication(RFC 1929)

    import socket
    import socks5
    import requests

    username, password = "bot", "123456"
    socks5.set_default_auth(username = username, password = password)
    socks5.set_default_socks_proxy("127.0.0.1:1080")
    socket.socket = socks5.SOCKS5

    test_url = "http://en.wikipedia.org"

    resp = requests.get(test_url)
    print resp.status_code
    print resp.headers["content-type"]
    print resp.text


## TODO

    support METHOD 0x02
    support CMD 0x01 and 0x02
    support HTTPS/FTP/telnet/whois/IAMP4
    confirms to RFC 1961 and 3089


## REFERENCES

 - RFC 1928, SOCKS Protocol V5
 - RFC 1929, Username/Password Authentication for SOCKS V5
 - RFC 1961, GSS-API Authentication Method for SOCKS Version 5
 - RFC 3089, A SOCKS-based IPv6/IPv4 Gateway Mechanism
