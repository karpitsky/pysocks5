#!/usr/bin/env python
import socket
import httplib
import socks5


#socks5.set_default_debug(1)
socks5.set_default_socks_proxy("127.0.0.1:7070")
socket.socket = socks5.SOCKS5


#host, port = "en.wikipedia.org", 80
host, port = "localhost", 80

conn = httplib.HTTPConnection(host = host, port = port)
conn.request(method = "GET", url = "/")
resp = conn.getresponse()
resp_body = resp.read()

print resp.status
print resp_body