#!/usr/bin/env python
try:
    from setuptools import setup
except ImportError:    
    from distutils.core import setup


short_desc = "RFC 1928 - SOCKS Protocol Version 5, Python implementation"

setup(
    name = "socks5",
    description = short_desc,
    version = "201202",
    author = "Shuge Lee",
    author_email = "shuge.lee@gmail.com",

    license = "BSD License",

    platforms = ["Mac OS X", "Linux"],
    url = "https://bitbucket.org/shugelee/pysocks5",

    classifiers = [
        "Development Status :: 4 - Beta",
        "Environment :: MacOS X",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2 :: Only",
        "Topic :: Internet",
        "Topic :: Internet :: Proxy Servers",
        ],

    packages = [
        "socks5",
        ],
)
